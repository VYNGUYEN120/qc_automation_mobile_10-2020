const event = require('codeceptjs').event
// require('./local_data')
exports.config = {
  rerun: {
    // run 6 times until 1st success
    minSuccess: 1,
    maxReruns: 6,
  },
  output: './output',
  helpers: {
    Appium: {
      platform: 'Android',
      device: 'Redmi',
      automationName: 'Appium',
      desiredCapabilities: {
        appPackage: 'com.leapxpert.manager.qa',// thong tin app
        appActivity: 'com.leapxpertapp.MainActivity',
        noReset: false,//giữ tiến trình trc
        fullReset: false,// reset lại tất cả trang thái
        automationName: 'UIAutomator2',//android  
        newCommandTimeout: 30000,// k set gia trị 600s 
      },
    },
    // Puppeteer: {// chay trinh duyet
    //   show: true,
    //   windowSize: '1536x864',
    //   chrome: {
    //     args: ['--ignore-certificate-errors'],
    //   },
    //   firefox: {
    //     args: ['--ignore-certificate-errors'],
    //   },
    // },
    REST: {
      endpoint: 'https://api.adaptavist.io/tm4j/v2',//tool tm4j thấy đc report
      onRequest: (request) => {},
    },
  },
  include: {
    I: './steps_file.js',
    LoginMobilePages: "./pages/login_pages_mobile.js", //namealiat
    LoginMobileStep:"./steps/login_step_mobile.js"
    
  },
  mocha: {
    reporterOptions: {
      mochaFile: 'output/result.xml',
      reportDir: 'output/result.html',
    },
  },
  // bootstrap: 'bootstrap.js',
  // teardown: 'bootstrap.js',
  hooks: [],
  plugins: {
    screenshotOnFail: {
      enabled: true,
    },
    retryFailedStep: {
      enabled: true,
    },
  },
  tests: './test/Login_test_mobile.js',
  name: 'lxp-web-automation',
}
