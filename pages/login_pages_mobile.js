const { I } = inject();

module.exports = {

  // insert your locators and methods here
  login: {
    updatebtn: '//android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[1]',
    laterbtn: '//android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]',
    skipBtn : '//android.view.ViewGroup[@content-desc="tutorial_skip"]/android.widget.TextView"]',
    code0 : '//android.widget.EditText[@content-desc="activation_0"]',
    code1 : '//android.widget.EditText[@content-desc="activation_1"]',
    code2 : '//android.widget.EditText[@content-desc="activation_2"]',
    code3 : '//android.widget.EditText[@content-desc="activation_3"]',
    code4 : '//android.widget.EditText[@content-desc="activation_4"]',
    code5 : '//android.widget.EditText[@content-desc="activation_5"]',
    code6 : '//android.widget.EditText[@content-desc="activation_6"]',
    code7 : '//android.widget.EditText[@content-desc="activation_7"]',
    code8 : '//android.widget.EditText[@content-desc="activation_8"]',
    code9 : '//android.widget.EditText[@content-desc="activation_9"]',
    passWord: '//android.widget.EditText[@content-desc="login_password"]',
    login : '//android.view.ViewGroup[@content-desc="login_signIn"]',
    otp1 : '//android.widget.EditText[@content-desc="otp_0"]',
    otp2 : '//android.widget.EditText[@content-desc="otp_1"]',
    otp3 : '//android.widget.EditText[@content-desc="otp_2"]',
    otp4 : '//android.widget.EditText[@content-desc="otp_3"]',
    otp5 : '//android.widget.EditText[@content-desc="otp_4"]',
    otp6 : '//android.widget.EditText[@content-desc="otp_5"]',
    userProfile : '//android.view.ViewGroup[@content-desc="chat_newChat"]'
    

  }
 
  // insert your locators and methods here
}
