const { setHeadlessWhen } = require('@codeceptjs/configure');
//const login_page = require('./pages/login_page');

// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

exports.config = {
  tests: './*_test.js',//ten file phai co _test
  output: './output',
  helpers: {
    Puppeteer: {
      url: 'https://github.com',
      show: true,
      windowSize: '1200x900'
    }
  },
  include: {
    I: './steps_file.js',
    Login_Pages: "./pages/login_page.js", //namealiat
    Login_Step:"./steps/login_step.js"
  },
  bootstrap: null,
  mocha: {},
  name: 'codeceptjs',
  plugins: {
    pauseOnFail: {},
    retryFailedStep: {
      enabled: true
    },
    tryTo: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true
    }
  }
}