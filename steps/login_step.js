const { I, Login_Pages } = inject();
//const LoginPageDemov= require('../pages/login_page')
module.exports = {

  // insert your locators and methods here
  login(username, email, password)
  {
    I.amOnPage('');
    I.waitForText('GitHub',10);
    I.seeElement(Login_Pages.login.userName);
    I.fillField(Login_Pages.login.userName, username);
    I.seeElement(Login_Pages.login.emailPath);
    I.fillField(Login_Pages.login.emailPath, email);
    I.seeElement(Login_Pages.login.passWord);
    I.fillField(Login_Pages.login.passWord, password);
    I.seeElement(Login_Pages.login.signUpButton);
    I.click(Login_Pages.login.signUpButton);
    
   
    
  }
}
