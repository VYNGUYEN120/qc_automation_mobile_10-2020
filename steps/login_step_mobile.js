const { I, LoginMobilePages } = inject();
//const LoginPageDemov= require('../pages/login_page')
module.exports = {

  // insert your locators and methods here
  login(qrCode, password, optCode)
  {
    
     I.waitForElement(LoginMobilePages.login.laterbtn,10);
     I.tap(LoginMobilePages.login.laterbtn);
     I.wait(10); 

     //I.waitForElement(LoginMobilePages.login.updatebtn,20);
     //I.tap(LoginMobilePages.login.updatebtn);
     //I.wait(30);
 
     I.waitForElement(LoginMobilePages.login.skipbtn, 10);
     I.tap(LoginMobilePages.login.skipBtn);
     I.wait(10);
 
     
     I.seeElement(LoginMobilePages.login.code0);
     I.fillField(LoginMobilePages.login.code0, qrCode[0]);
     I.seeElement(LoginMobilePages.login.code1);
     I.fillField(LoginMobilePages.login.code1, qrCode[1]);
     I.seeElement(LoginMobilePages.login.code2);
     I.fillField(LoginMobilePages.login.code2, qrCode[2]);
     I.seeElement(LoginMobilePages.login.code3);
     I.fillField(LoginMobilePages.login.code3, qrCode[3]);
     I.seeElement(LoginMobilePages.login.code4);
     I.fillField(LoginMobilePages.login.code4, qrCode[4]);
     I.seeElement(LoginMobilePages.login.code5);
     I.fillField(LoginMobilePages.login.code5, qrCode[5]);
     I.seeElement(LoginMobilePages.login.code6);
     I.fillField(LoginMobilePages.login.code6, qrCode[6]);
     I.seeElement(LoginMobilePages.login.code7);
     I.fillField(LoginMobilePages.login.code7, qrCode[7]);
     I.seeElement(LoginMobilePages.login.code8);
     I.fillField(LoginMobilePages.login.code8, qrCode[8]);
     I.seeElement(LoginMobilePages.login.code9);
     I.fillField(LoginMobilePages.login.code9, qrCode[9]);
     I.tap(LoginMobilePages.login.nextbtn);
     I.wait(10);
 
     I.seeElement(LoginMobilePages.login.passWord);
     I.fillField(LoginMobilePages.login.passWord, password)
     I.tap(LoginMobilePages.login.loginbtn)
     I.wait(10);

     I.seeElement(LoginMobilePages.login.otp1);
     I.fillField(LoginMobilePages.login.otp1, optCode[0]);
     I.seeElement(LoginMobilePages.login.otp2);
     I.fillField(LoginMobilePages.login.otp2, optCode[1]);
     I.seeElement(LoginMobilePages.login.otp3);
     I.fillField(LoginMobilePages.login.otp3, optCode[2]);
     I.seeElement(LoginMobilePages.login.otp4);
     I.fillField(LoginMobilePages.login.otp4, optCode[3]);
     I.seeElement(LoginMobilePages.login.otp5);
     I.fillField(LoginMobilePages.login.otp5, optCode[4]);
     I.seeElement(LoginMobilePages.login.otp6);
     I.fillField(LoginMobilePages.login.otp6, optCode[5]);
     I.tap(LoginMobilePages.login.confirmbtn);
     I.wait(20);
 
 
     I.waitForElement(LoginMobilePages.login.userProfile, 10);
     I.tap(LoginMobilePages.login.userProfile);
     I.wait(10);
 
   }
   
 }
    
    
   
    
  